#!/usr/bin/env python
# -*- coding: utf-8 -*-

__licence__ = """
This file is part of stats.py.

stats.py is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

stats.py is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with stats.py. If not, see <http://www.gnu.org/licenses/>.
"""
__author__ = "Corentin Rossignon"


import sys, re
import numpy as np

# number_regex = re.compile( r"[-+]?\d*\.?\d+(?:[eE][-+]?\d+)?(?:\s|$)" )
number_regex = re.compile( r"[-+]?\d*\.?\d+(?:[eE][-+]?\d+)?" )

def try_to_integer(value) :
    if value == int(value) :
        return int(value)
    return value

def my_stats(nums) :
    if min(nums) == max(nums) :
       return str(try_to_integer(nums[0]))
    return "%s-%s(%s:%s)" % (try_to_integer(min(nums)), try_to_integer(max(nums)), try_to_integer(np.median(nums)), try_to_integer(np.average(nums)))


def process_file(filename, table, order) :
    if filename == "-" :
        file_to_read = sys.stdin
    else :
        file_to_read = open(filename, "r")

    for line in file_to_read :
        line = line.replace("%", "%%")
        pattern = "%s".join(number_regex.split(line))
        numbers = map(lambda x : float(x), number_regex.findall(line))

        if pattern not in table :
            order.append(pattern)
            table[pattern] = [ numbers ]
        else :
            table[pattern].append(numbers)

def compute_avg(list_numbers) :
    return tuple(map(lambda nums : my_stats(nums), zip(*list_numbers)))
    return tuple(map(lambda nums : str(np.median(nums)), zip(*list_numbers)))
    return tuple(map(lambda nums : str(np.average(nums)), zip(*list_numbers)))
    return tuple(map(lambda nums : str(nums), zip(*list_numbers)))


def main() :
    table = {}
    order = []
    if len(sys.argv) == 1 :
        process_file("-", table, order)
    else :
        for filename in sys.argv[1:] :
            process_file(filename, table, order)

    for pattern in order :
        sys.stdout.write(pattern % compute_avg(table[pattern]))

if __name__ == "__main__" :
    main()
